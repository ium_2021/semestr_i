public class AC {

    public int bits;
    public double samplingFreq;
    public double rangeMin, rangeMax;
    public double sensitivity;
    public int channelNr;
    public String unit;
    public String channelName;


    public int length;
    public short[] samples;

    public AC(int channelNr, int bits, double rangeMin, double rangeMax, double samplingFreq, short[] samples, String unit, String channelName)
    {
        this.channelNr = channelNr;
        this.bits = bits;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.sensitivity = (rangeMax-rangeMin)/(Math.pow(2,bits)-1); //obliczenie czułości
        this.samplingFreq = samplingFreq;
        this.samples = samples;
        this.channelName = channelName;
        this.unit = unit;
        this.length = this.samples.length;
    }



    public double value(int i)
    {
        if(i >= 0 && i < this.length)
        {
            return this.sensitivity*this.samples[i]+this.rangeMin;
        }

        else
        {
            return Double.NaN; //w przypadku błędu wypełnić pustą wartością
        }
    }

    public double mean()
    {
        double result = 0.0;
        for(int i = 0; i < this.length; i++)
        {
            result =  result + this.value(i);
        }
        return result/this.length;
    }

    public double rms()
    {
        double result = 0.0;
        for(int i = 0; i < this.length; i++)
        {
            result =  result + Math.pow(this.value(i),2);
        }
        return Math.pow(result/this.length,0.5);
    }

    public String toString() // Klasa string wypisująca wszystkie wartości sygnału
    {
        String result = "Channel:\t\t" + this.channelName + "\nChanner No.:\t" + this.channelNr + "\nNo. of Bits:\t" + this.bits +
                "\nNo. of Samples:\t" + this.length + "\nUnit:\t\t\t" + this.unit + "\nSensitivity:\t" + this.sensitivity + " " + this.unit +
                "\nMin read:\t\t" + this.rangeMin + "\nMax read:\t\t" + this.rangeMax + "\nSampling Freq:\t" + this.samplingFreq +
                " Hz\nMean:\t\t\t" + this.mean() + " " + this.unit + "\nRMS:\t\t\t" + this.rms() + " " + this.unit + "\nValues [" + this.unit + "]:";

        for(int i = 0; i < this.length; i++)
        {
            result += "\n" + ".\t" + this.value(i);
        }

        return result;
    }
}