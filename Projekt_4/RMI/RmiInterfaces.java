package rmiversion1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiInterfaces extends Remote {
	boolean register(Object obj) throws RemoteException;

	String getRegister(Dane dataType, String y) throws RemoteException;

	String saveObject(Dane dataType) throws RemoteException;

	Object getObject(Dane dataType, int listNumber) throws RemoteException;
}
