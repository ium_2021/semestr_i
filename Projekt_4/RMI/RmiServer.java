package rmiversion1;

import types.Packet;
import types.Spectrum;
import types.TimeHistory;
import java.util.ArrayList;
import java.util.List;
import java.rmi.registry.Registry;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;


import types.*;


public class RmiServer implements RmiInterfaces {

	private final List<TimeHistory> TimeHistoryList = new ArrayList<>();
	private final List<Spectrum> SpectrumList = new ArrayList<>();


	public static void main(String[] args) throws AlreadyBoundException {
		// TODO Auto-generated method stub
		Registry reg = null;
		//RmiServant servant;
		try {
			reg = LocateRegistry.createRegistry(9876);

		}catch (RemoteException e) {
			e.printStackTrace();
			
		}
		try {
			RmiServer rs = new RmiServer();
			RmiInterfaces serv = (RmiInterfaces) UnicastRemoteObject.exportObject( rs, 0);
			assert reg != null;
			reg.bind("RmiServer", serv);
			System.out.println("RmiServer ready");


		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		}
	

	
	public Object getObject(Dane dataType, int listNumber) {

		Object rs = null;
		
		switch (dataType) {
		case TIMEHISTORY: {
			if(listNumber <= TimeHistoryList.size() && listNumber > 0)
				rs = TimeHistoryList.get(listNumber-1);
			else
				System.out.println("wrong number of THlist");
		}
		case SPECTRUM: {
			if(listNumber <= SpectrumList.size() && listNumber > 0)
				rs = SpectrumList.get(listNumber-1);
			else
				System.out.println("wrong number of SPlist");
		}	
		}
		return rs;
		
		
	}



	public String saveObject(Dane dataType) {
		switch (dataType) {
		case TIMEHISTORY:
		{
				TimeHistory<?> object = TimeHistoryList.get(TimeHistoryList.size() - 1);
				//System.out.println("you are here");
				//System.out.println("Object:" + object);
				try {
					
					 
					FileOutputStream fileOut = new FileOutputStream(object.toPath() + ".thi");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(object);
					objectOut.close();
					System.out.println("\nSaved File: " + object.toString());
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			
			break;
		}
		case SPECTRUM:
		{
				Spectrum<?> object = SpectrumList.get(SpectrumList.size() - 1);
				try {
					FileOutputStream fileOut = new FileOutputStream(object.toPath() + ".spc");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(object);
					objectOut.close();
					System.out.println("\nSaved File: " + object.toString());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			
			break;
		}
	
		}
		return "Last entered object was saved to files";
	}

	public boolean register(Object obj) {
		
		System.out.println("registration of the file in to the server list");
		
		if(obj.getClass().equals(TimeHistory.class)) {
			TimeHistoryList.add((TimeHistory<?>) obj);
		}else {
			SpectrumList.add((Spectrum<?>) obj);
		}
		
		return true;
		
	}

	public String getRegister(Dane dataType, String y) {
		String feedback = null;
		if(dataType == Dane.TIMEHISTORY) {
			feedback = "TimeHistory List:";
			int i = 1;
		for(int j = 1; j<= TimeHistoryList.size();j++) {
		if(TimeHistoryList.get(i-1).toString().matches("(.*)"+y+"(.*)")) {	
		feedback = feedback + "\n" + i + ". " + TimeHistoryList.get(i++-1);
		}
		}
		}
		else {
			feedback = "Spectrum List:";
			int i = 1;
			for(int j = 1; j<= SpectrumList.size();j++) {
				if(SpectrumList.get(i-1).toString().matches("(.*)"+y+"(.*)")) {
				feedback = feedback + "\n" + i + ". " + SpectrumList.get(i++-1);
				}
				}
		}
		return feedback;
		

	}
}
