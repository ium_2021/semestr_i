package rmiversion1;


import types.Spectrum;
import types.TimeHistory;
import java.util.Date;
import java.rmi.registry.Registry;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;





public class RmiClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length < 1) {
			System.out.println(" Usage : RmiClient <server host name >");
			System.exit(-1);
		}
		
		Double[] dataX = {1.d,2.d,3.d,4.d};
		Character[] dataXX = {'a','b','c','d'};
		
		//TimeHistory<Double> dane1 = new TimeHistory<Double>("device_9","description_9",(new Date()).getTime(),37,"Volt_6",0.122,dataX,321.321);
		Spectrum<Character> dane2 = new Spectrum<Character>("device_9","description_9",(new Date()).getTime(),37,"Volt_6",0.122,dataXX,"linear");
		//Dane dataType = Dane.TIMEHISTORY;
		Dane dataType = Dane.SPECTRUM;
		String searchInfo = "device";
		
		
		try {
			Registry reg = LocateRegistry.getRegistry(9876);
			RmiInterfaces serv = (RmiInterfaces) reg.lookup("RmiServer");
			System.out.println("Client started");
			boolean daneRegister = serv.register(dane2);
			if (daneRegister) {
				System.out.println("Data successfully written to server's list");
		}
		int index = 1;
		String data = serv.getRegister(dataType, searchInfo);
		System.out.println(data);
		String response = serv.saveObject(dataType);
		System.out.println(response);
		Object receivedObject = serv.getObject(dataType, index);
		System.out.println("The object was obtained from the list with the selected index: \n"+ index + ". " + receivedObject.toString());

		
	//	System.out.println("\n" + dane2.toString());
	//	if(dane2.toString().matches("(.*)device(.*)")) System.out.println("match");
	//	else System.out.println("no match");
		//
		}catch(RemoteException e) {
			e.printStackTrace();
		}catch(NotBoundException e) {
			e.printStackTrace();
		}
	}
}