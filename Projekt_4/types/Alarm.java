package types;

import java.util.Date;

public class Alarm extends Packet{
	
	private int channelNr;
	private double threshold;
	private int direction;
	
	public Alarm(String device, String description, long date, int channelNr, double threshold, int direction) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.threshold = threshold;
		this.direction = direction;
	}
	
	public Alarm() {
		this.device = "device_3";
		this.description = "description_3";
		this.date = (new Date()).getTime();
		
		this.channelNr = 3;
		this.threshold = 500;
		this.direction = 0;
	}

	@Override
	public String toString() {
		return "Alarm [channelNr=" + channelNr + ", threshold=" + threshold + ", direction=" + direction + ", device="
				+ device + ", description=" + description + ", date=" + date + "]";
	}
	
	

}
