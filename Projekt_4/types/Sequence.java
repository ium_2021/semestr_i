package types;
import java.util.Arrays;

public abstract class Sequence<T> extends Packet {

	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T[] buffer;
	
	
	


	public Sequence(String device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = buffer;
	}
	

	public Sequence(String device, String description, long date, int channelNr, String unit, double resolution,int len) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		buffer = (T[]) new Object[len];
		
		
	}

	protected int getLength() {
		return buffer.length;
	}
	
	public void set(T[] values) {
		int i = 0;
		for(T v : values)
			buffer[i++] = v;
	}

	@Override
	public String toString() {
		return "Sequence [channelNr=" + channelNr + ", unit=" + unit + ", resolution=" + resolution + ", buffer="
				+ Arrays.toString(buffer) + ", device=" + device + ", description=" + description + ", date=" + date
				+ "]";
	}

	public String getNameInformation(){
		
		return device+ " ChNr_" + channelNr + " Time_" + date;
	}
	
	
}
