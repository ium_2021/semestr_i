package types;
import java.util.Arrays;

public class TimeHistory<T> extends Sequence<T> {

	protected double sensitivity;

	public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer, double sensitivity) {
		super(device, description, date, channelNr, unit, resolution, buffer);
		this.sensitivity = sensitivity;
	}
	public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer) {
		super(device, description, date, channelNr, unit, resolution,buffer);
		
	}

	public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution,
			int len) {
		super(device, description, date, channelNr, unit, resolution,len);
		
	}

	@Override
	public String toString() {
		return "TimeHistory [sensitivity=" + sensitivity + ", channelNr=" + channelNr + ", unit=" + unit
				+ ", resolution=" + resolution + ", buffer=" + Arrays.toString(buffer) + ", device=" + device
				+ ", description=" + description + ", date=" + date + "]";
	}
	public String getNameInformation(){
		
		return device+ " ChNr_" + channelNr + " Time_" + date;
	}
	
	
	
	
}
