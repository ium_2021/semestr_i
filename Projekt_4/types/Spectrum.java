package types;
import java.util.Arrays;

public class Spectrum<T> extends Sequence<T> {
protected String scaling;

public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution,
		T[] buffer, String scalin) {
	super(device, description, date, channelNr, unit, resolution, buffer);
	this.scaling = scaling;
}


	
public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution,
		int len) {
	super(device, description, date, channelNr, unit, resolution,len);
	
}





@Override
public String toString() {
	return "Spectrum [scaling=" + scaling + ", channelNr=" + channelNr + ", unit=" + unit + ", resolution=" + resolution
			+ ", buffer=" + Arrays.toString(buffer) + ", device=" + device + ", description=" + description + ", date="
			+ date + "]";
}



}
