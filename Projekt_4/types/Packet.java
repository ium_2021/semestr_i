package types;

import java.io.Serializable;

public abstract class Packet implements Serializable {
	
	protected String device;
	protected String description;
	protected long date;

	public Packet(String device, String description, long date) {
		
		this.device = device;
		this.description = description;
		this.date = date;
	}
	
	public Packet(){
		this.device = "device_1";
		this.description = "description_1";
		this.date = 123456;
		
	}

	@Override
	public String toString() {
		return "Packet [device= " + device + ", description= " + description + ", date= " + date + "]";
	}
	
public String getNameInformation(){
		
		return  device + " " + date + " " + description;
	}
public String toPath() {
	return "C:\\Users\\BARTEK\\Desktop\\SEM 8\\Java_ImW\\Cw_3\\" + device + "_" + description + "_" + date;
}
	
}
