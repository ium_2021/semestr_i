package udpversion2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

import tools.Tools;
import types.Packet;
import types.Spectrum;
import types.TimeHistory;

public class UdpClient {

	public static void main(String[] args) {
		InetAddress aHost = null;
		int serverPort = 7789;

		DatagramSocket aSocket = null;


		try {

			if (args.length < 1) {
				System.out.println("Usage: UdpClient <server host name>");
				System.exit(-1);
			}

			byte[] buffer = new byte[10242];
			aHost = InetAddress.getByName(args[0]);
			aSocket = new DatagramSocket();

			Double[] dataX = { 1.d, 2.d, 3.d, 4.d };
			Character[] dataXX = { 'a', 'b', 'c', 'd' };

			TimeHistory<Double> th = new TimeHistory<Double>("device_9", "description_9", (new Date()).getTime(), 37,
					"Volt_6", 0.122, dataX, 321.321);
			Spectrum<Character> sp = new Spectrum<Character>("device_9", "description_9", (new Date()).getTime(), 37,
					"Volt_6", 0.122, dataXX, "linear");

			Request object = new Request("device_5", "description_1", (new Date()).getTime(), null, 0, null, "");

			object.setType(Request.Type.SEARCH);


			object.setValue(0);


			object.setDevice("device_5");

			object.setObject(th);



			byte[] data = Tools.serialize(object);


			switch (object.getType()) {
			case SEARCH:
				System.out.println("You want to SEARCH all devices where: " + object.getSelectedDevice());
				break;
			case LIST:
				System.out.println("You want to LIST all defices ");
				break;
			case SAVE:
				System.out.println("You want to SAVE: " + object.getNameInformation());
				break;
			}



			DatagramPacket dataP = new DatagramPacket(data, data.length, aHost, serverPort);
			aSocket.send(dataP);

			System.out.println("Object send to server");

			DatagramPacket reply = new DatagramPacket(buffer, buffer.length);

			aSocket.receive(reply);

			System.out.println("Object receive from server");

			String read = new String(reply.getData(), 0, reply.getLength());


			System.out.println("Reply: " + read.toString());

		}


		catch (SocketException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			System.out.println("program completed.");
			aSocket.close();


		}

	}

}
