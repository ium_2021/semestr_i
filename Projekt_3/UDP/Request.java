package udpversion2;


import tools.Tools;
import types.Packet;
import types.Spectrum;
import types.TimeHistory;
import java.io.Serializable;


public class Request extends Packet implements Serializable  {
	
	public enum Type{
		SAVE,
		LIST,
		SEARCH
	}
	
	private Type type;
	private int value;
	private Object object;
	private String selectedDevice;
	
	
	public Request() {
		super("","",0);
		type = Type.SAVE;
		value = 0;
		object = null;
		selectedDevice = "";
	}
	
	public Request(String device, String description, long date, Type type, int value, Object object, String selectedDevice) {
		super(device, description, date);
		this.type = type;
		this.value = value;
		this.object = object;
		this.selectedDevice = selectedDevice;
	}
	
	
public Type getType() {
	return type;
}
public void setType(Type type) {
	this.type = type;
}
public void setValue(int value) {
	this.value = value;
}
public void setObject(Object object){
	this.object = object;
}
public Object getObject() {
	return object;
}
public int getValue() {
	return value;
}
public String getSelectedDevice() {
	return selectedDevice;
}
public void setDevice(String selected) {
	this.selectedDevice = selected;  // why I cant: this.selectedDevice = selectedDevice ???
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}

}
