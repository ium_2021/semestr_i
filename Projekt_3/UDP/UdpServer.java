package udpversion2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import tools.Tools;
import types.Packet;
import types.Spectrum;
import types.TimeHistory;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UdpServer {

	public static void main(String[] args) {
		//server time in seconds from start server
		
				Thread thread = new Thread(()-> {
					int i = 0;
					while(true) {
						System.out.println(i++);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {

							e.printStackTrace();
						}
					}
				});
				thread.start();
				
				
		DatagramSocket aSocket = null;
		try {

			aSocket = new DatagramSocket(7789);
			byte[] buffer = new byte[10242];

			String feedback = "";
			while (true) {

				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				System.out.println("Waiting for request...");
				aSocket.receive(request);

				Request read = (Request) Tools.deserialize(request.getData());
				System.out.println("Object info: " + read.getObject());
				System.out.println("NameInfo: " + read.getNameInformation());
				System.out.println("SetInfo/Value: " + read.getType() + " / " + read.getValue());


				File myObj = new File("C:/Users/BARTEK/Desktop/SEM 8/Java_ImW/Cw_2");
				File[] files = null;

				switch (read.getType()) {
				case SAVE:

					Files.write(new File(read.getNameInformation() + ".txt").toPath(), request.getData());

					System.out.println("Obiect saved as: " + read.getNameInformation());

					feedback = "Obiekt zapisany";
					break;

				case LIST:
					int howManyList = 0;

					files = myObj.listFiles();
					feedback = "\n Files list: ";
					if (files != null && files.length > 0) {
						for (File file : files) {
							if (file.isFile() && !file.getName().startsWith(".")) {
								feedback = feedback + "\n" + file.getName();
								howManyList++;
							}
						}
					}
					feedback = feedback + "\nSelected " + howManyList + " devices.";

					break;

				case SEARCH:
					int howManySearch = 0;
					files = myObj.listFiles();
					feedback = "\n Files list: ";
					boolean found = false;
					if (files != null && files.length > 0) {
						for (File file : files) {
							if (file.isFile() && !file.getName().startsWith(".")
									&& read.getSelectedDevice().length() <= file.getName().length()) {

								for (int i = 0; i < read.getSelectedDevice().length(); i++) {
									
									if (read.getSelectedDevice().charAt(i) == file.getName().charAt(i))
										
										found = true;
									else
										found = false;
								}

								if (found == true) {
									///

									InputStream inputStream = null;
									try {

										inputStream = new FileInputStream(file);

										// ...
										DatagramPacket request2 = new DatagramPacket(inputStream.readAllBytes(),
												inputStream.readAllBytes().length);
										Request read2 = (Request) Tools.deserialize(request2.getData());
										System.out.println(read2);

										feedback = feedback + "\n" + file.getName() + "\n" + read2.getObject();
										
									} finally {
										if (inputStream != null) {
											try {
												inputStream.close();
											} catch (IOException e) {
												e.printStackTrace();
											}
										}
									}
									System.out.println("selected: " + file.getName());
									howManySearch++;
									///
								}
								found = false;
							}

						}
					}
					feedback = feedback + "\nSelected " + howManySearch + " devices.";
					break;

				default:
					feedback = "choose SAVE/LIST/SEARCH";
					break;
				}

				DatagramPacket reply = new DatagramPacket(feedback.getBytes(), feedback.length(), request.getAddress(),
						request.getPort());
				aSocket.send(reply);


			}
		} catch (ClassNotFoundException c) {
			System.out.println("data not found");
			c.printStackTrace();
		} catch (SocketException ex) {
			Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
		}
	}
}
