package udpversion1;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;


import tools.Tools;
import types.Packet;
import types.Spectrum;
import types.TimeHistory;



public class UdpClient{
	
	public static void main(String[] args) {
		InetAddress aHost = null;
		int serverPort = 9876;
		
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		
	
		
		
		try {
			// args contain server hostname
			//System.out.println(args.length);
			if (args.length < 1) {
				System.out.println("Usage: UdpClient <server host name>");
				System.exit(-1);
			}
			byte[] 		buffer = new byte[1024];
			byte[]		bufferHistory = new byte[1024];
		    aHost = InetAddress.getByName(args[0]);
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			String line = "";
			////////
			Double[] dataX = {1.d,2.d,3.d,4.d};
			
			TimeHistory<Double> packet = new TimeHistory<Double>("device_9","description_9",(new Date()).getTime(),37,"Volt_6",0.122,dataX,321.321);
			byte[] data = Tools.serialize(packet);
			
		
			
			DatagramPacket dataP = new DatagramPacket(data,data.length,aHost,serverPort);
			aSocket.send(dataP);
			
			System.out.println("Object send");
			
			DatagramPacket replyHistory = new DatagramPacket(bufferHistory, bufferHistory.length);	
			
		
			aSocket.receive(replyHistory);
			
			System.out.println("Object receive from server");
	
			
			Packet read;
			try {
			
			read = (Packet) Tools.deserialize(replyHistory.getData());
			System.out.println("Reply: " + read.toString());
			}
			catch (ClassNotFoundException c) {
				System.out.println("data not found");
				c.printStackTrace();
			}

		
		
			
			////////
		
			while (true) {
				System.out.println("Enter your message: ");
				if (scanner.hasNextLine())
					line = scanner.nextLine();
				DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
				aSocket.send(request);
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);
				System.out.println("Reply: " + new String(reply.getData(), 0, reply.getLength()));
			}
		} catch (SocketException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			System.out.println("D");
			aSocket.close();
			scanner.close();
	
		}
		
		
	
	
		
		
		
	}
}