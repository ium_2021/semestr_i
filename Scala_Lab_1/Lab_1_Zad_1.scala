package Scala_1

object Hello {
  def main(args: Array[String]): Unit = {

    println("Ustaw ilość kolumn: ")
    var kolumny: Int = scala.io.StdIn.readLine().toInt
    printf("Ustaw ilość rzędów: ")
    var rzedny: Int = scala.io.StdIn.readLine().toInt

    println()
    for (n <- 1 to rzedny) {
      for (m <- 1 to kolumny) {
        print("*")

      }
      print("*")
      println(" ")
    }

  }

}
