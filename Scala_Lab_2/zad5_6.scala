package Scala_1
abstract class Worker(val name: String){
  def call: String
}


class Peon(name: String) extends Worker(name) {
  def call(): String = {
    "Praca Praca"
  }
}
  trait Data{
    val age:Int
    val salary:Double
  }
 class NormalWorker (name: String, val age:Int, val salary:Double) extends Peon(name) with Data

class OldWorker (name: String, val age:Int, val salary:Double) extends Peon(name) with Data

object employees extends App{
  val worker1 = new Worker(name = "Sadownik") {
    override def call: String = "Najlepsze podatki, to brak podatków"
  }
  val worker2 = new Peon(name = "Ork")

  val worker3 = new NormalWorker(name = "Pawel",age = 21,salary = 2000.5)
  val worker4 = new OldWorker(name = "Stary Pawel", age = 200, salary = 200000.34342)
  val workers = Vector(worker1,worker2,worker3,worker4)

    println(workers)
  val workers_list = List((worker1,worker2,worker3,worker4))
}
