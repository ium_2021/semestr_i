

abstract class Alpha


case class Bravo() extends Alpha

case class Charlie() extends Alpha

object Alpha extends App{
  val Xray :Int = 2

  Xray match{
    case 1 => println("1")
    case 2 => println("2")
    case 3 => println("3")
  }
    val Yankee = Bravo()

  def matchAlpha(alpha: Alpha):Unit = {
    alpha match {
      case Bravo() => println("its Bravo")
      case Charlie() => println("Its Charlie")
    }
  }
  matchAlpha(Yankee)
}