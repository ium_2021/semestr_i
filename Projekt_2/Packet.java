
public abstract class Packet {
    protected String device;
    protected String description;
    protected long date;

    Packet(){
        device = "Example Device";
        description = "Example description";
        date = System.currentTimeMillis();
    }

    Packet(String device, String description, long date){
        this.device = device;
        this.description = description;
        this.date = date;
    }

    public String toString(){
        return "\nDevice name:\t" + device + "\nDescription:\t" + description + "\nDate:\t\t\t" + date;
    }
}