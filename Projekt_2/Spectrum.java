public class Spectrum extends Sequence{
    private boolean scaling;//false - linearny przebieg, true - przebieg logarytmiczny

    Spectrum(){
        super();
        scaling = false;
    }

    Spectrum(String device, String description, long date, int channelNr, String unit, double resolution, Object[] buffer, boolean scaling){
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.scaling = scaling;
    }

    public String toString(){
        if(scaling) {
            return super.toString() + "\nScaling:\tlogarythmic";
        }
        else{
            return super.toString() + "\nScaling:\tlinear";
        }
    }
}