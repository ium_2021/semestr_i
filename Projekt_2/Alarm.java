public class Alarm extends Packet{
    private int channelNr;
    private Object threshold;
    private int direction;

    Alarm() {
        super();
        channelNr = 32;
        threshold = 0;
        direction = -1;

    }

    Alarm(String device, String description, long date, int channelNr, Object threshold, int direction){
        super(device, description, date);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    public String toString(){
        return super.toString() + "\nChannel number:\t" + channelNr + "\nThreshold:\t\t" + threshold + "\nDirection:\t\t" + direction;
    }
}