import java.util.ArrayList;

public abstract class Sequence extends Packet {
    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected Object[] buffer;

    Sequence(){
        super();
        channelNr = -1;
        unit = "U";// unspecified
        resolution = 1.0;
        buffer = new Integer[]{0,1};
    }

    Sequence(String device, String description, long date, int channelNr, String unit, double resolution, Object[] buffer){
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    public String toString(){
        String result = super.toString() + "\nChannel number:\t" + channelNr + "\nUnit:\t\t\t" + unit + "\nResolution:\t\t" + resolution + "\nBuffer:";
        for(int i = 0; i < buffer.length; i++){
            result = result + "\n\t" + i + ".\t" + buffer[i];
        }
        return result;
    }
}