public class TimeHistory extends Sequence {
    private double sensitivity;

    TimeHistory(){
        super();
        sensitivity = 0.50;
    }

    TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution, Object[] buffer, double sensitivity){
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.sensitivity = sensitivity;
    }

    public String toString(){
        return super.toString() + "\nSensitivity:\t" + sensitivity;
    }
}